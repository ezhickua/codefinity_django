# codefinity_django

Django project

installed app:
python
pylance
pylint
AutoRenameTag
BlackFormater
CSS Peak
Django (Baptiste Darthenay)
Flake8
IntelliSense for CSS
LiveServer
Prettier

settings.json:

"terminal.integrated.defaultProfile.windows": "Command Prompt",

{
"workbench.colorTheme": "Visual Studio Dark",
"files.autoSave": "afterDelay",
"terminal.integrated.cursorStyle": "line",
"terminal.integrated.cursorBlinking": true,
"explorer.compactFolders": false,
"liveServer.settings.donotShowInfoMsg": true,
"editor.formatOnSave": true,
"workbench.startupEditor": "none",
"git.enableSmartCommit": true,
"git.confirmSync": false,
"git.autofetch": true,
"explorer.confirmDelete": false,
"[json]": {
"editor.defaultFormatter": "esbenp.prettier-vscode"
},
"[html]": {
"editor.defaultFormatter": "esbenp.prettier-vscode"
},
"[css]": {
"editor.defaultFormatter": "esbenp.prettier-vscode"
},
"editor.linkedEditing": true,
"telemetry.telemetryLevel": "off",
"files.trimFinalNewlines": true,
"files.trimTrailingWhitespace": true,
"python.languageServer": "Pylance",
"python.analysis.typeCheckingMode": "off",
"python.analysis.diagnosticMode": "workspace",
"python.analysis.autoSearchPaths": true,
"python.analysis.autoImportCompletions": true,
"python.analysis.completeFunctionParens": true,
"python.analysis.inlayHints.variableTypes": true,
"python.analysis.inlayHints.functionReturnTypes": true,
"python.analysis.importFormat": "absolute",
"python.analysis.enablePytestSupport": true,
"python.analysis.indexing": true,
"python.analysis.packageIndexDepths": [
{
"name": "django",
"depth": 3,
"includeAllSymbols": true
},
{
"name": "flask",
"depth": 3,
"includeAllSymbols": true
},
{
"name": "fastapi",
"depth": 3,
"includeAllSymbols": true
}
],
"[python]": {
"editor.defaultFormatter": "ms-python.black-formatter"
},
"emmet.includeLanguages": {
"django-html": "html"
},
"files.associations": {
"**/\*.html": "html",
"**/templates/_/_.html": "django-html",
"**/templates/_/_/\*.html": "django-html",
"**/templates/_": "django-txt",
"**/requirements{/**,_}.{txt,in}": "pip-requirements"
},
"django.snippets.exclude": ["cms", "wagtail"],
"[django-html]": {
"editor.defaultFormatter": "batisteo.vscode-django",
"breadcrumbs.showClasses": true,
"editor.quickSuggestions": {
"other": true,
"comments": true,
"strings": true
}
}
}


#### python -Xutf8 manage.py dumpdata catalog.Products --format json --output=fixtures/catalog/product2.json

#### python manage.py loaddata fixtures/catalog/product2.json