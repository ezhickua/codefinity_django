from django.contrib import admin
from catalog.models import Categories, Products

# Register your models here.

# admin.site.register(Categories)

# admin.site.register(Products)


@admin.register(Categories)
class AdminCategory(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ["name", "slug"]
    list_display_links = ["name"]
    search_fields = ["name"]
    list_filter = ("name",)
    actions_on_bottom = True


@admin.register(Products)
class AdminProducts(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ["name", "slug"]
    list_display_links = ["name"]
    search_fields = ["name"]
    list_filter = ("name",)
    actions_on_bottom = True
