from typing import Any
from django.db import models


class Categories(models.Model):
    name: str = models.CharField(
        verbose_name="Назва категорії", max_length=250, unique=True
    )
    slug: str = models.SlugField(
        verbose_name="Посилання slug",
        max_length=150,
        unique=True,
        blank=True,
        null=True,
    )

    class Meta:
        db_table: str = "category"
        verbose_name_plural: str = "Категорії"
        verbose_name: str = "Категорію"

    def __str__(self):
        return self.name


class Products(models.Model):
    name: str = models.CharField(
        verbose_name="Назва товару", max_length=250, unique=True
    )
    slug: str = models.SlugField(
        verbose_name="Посилання slug",
        max_length=150,
        unique=True,
        blank=True,
        null=True,
    )
    price: float = models.DecimalField(
        decimal_places=2, max_digits=7, default=0.00, verbose_name="Ціна"
    )
    discount: float = models.DecimalField(
        decimal_places=2, max_digits=7, default=0.00, verbose_name="Скідка"
    )
    quantity: int = models.PositiveIntegerField(default=0, verbose_name="Кількість")
    description: str = models.TextField(blank=True, null=True, verbose_name="Опис")
    categories = models.ForeignKey(
        to=Categories, on_delete=models.PROTECT, verbose_name="Категорія"
    )
    # categories: models.ForeignKey(
    #     to=Categories, on_delete=models.CASCADE, verbose_name="Категорія"
    # )
    # categories: models.ForeignKey(
    #     to=Categories, on_delete=models.SET_DEFAULT, default="Різне", verbose_name="Категорія"
    # )
    image = models.ImageField(
        upload_to="product_images", blank=True, null=True, verbose_name="Зображення"
    )

    class Meta:
        """Change admin console"""

        db_table: str = "product"
        verbose_name_plural: str = "Товари"
        verbose_name: str = "Товар"

    def __str__(self):
        """Reload method __str__"""
        return self.name

    def display_id(self):
        return f"{self.id:05}"

    def sell_price(self):
        if not self.discount:
            return self.price
        return round(self.price - (self.price * self.discount / 100), 2)
