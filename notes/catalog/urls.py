from django.urls import path
from . import views

app_name = "catalog"

urlpatterns = [
    path("<slug:category_slug>", views.index, name="index"),
    path("product/<slug:product_slug>", views.detail_product, name="detail"),
    # path("catalog/<int:year>/<int:month>/<slug:slug>/", views.article_detail),
]
