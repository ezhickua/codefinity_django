from django.shortcuts import render, get_list_or_404

from catalog.models import Products

# Create your views here.


def index(requests, category_slug):
    if category_slug == "vsi-tovari":
        items = Products.objects.all()
    else:
        items = get_list_or_404(Products.objects.filter(categories__slug=category_slug))

    context: dict[str : int | str] = {
        "title": "Каталог интернет магазина мебели",
        "content": "Новинки только у нас",
        "items": items,
    }
    return render(requests, "catalog/shop.html", context=context)


def detail_product(requests, product_slug):
    product = Products.objects.get(slug=product_slug)
    return render(requests, "catalog/product.html", context={"product": product})
