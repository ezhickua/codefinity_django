from django.http import HttpResponse
from django.shortcuts import render

from catalog.models import Categories


# Create your views here.


def index(request) -> HttpResponse:
    context: dict[str : int | str] = {
        "title": "Интернет магазин мебели",
        "content": "Современный интерьер",
    }
    return render(request, "main/index.html", context=context)


def about(request) -> HttpResponse:
    context: dict[str : int | str] = {
        "title": "О нас",
        "content": "Современный интерьер",
    }
    return render(request, "main/about.html", context=context)
